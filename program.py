import cv2
import math
import numpy as np
import matplotlib.pyplot as plt

img = [0,0,0,0,0,0]
cnt = [0,0,0,0,0,0]
values = [0,0,0,0,0,0]

#wstępna obróbka obrazu, zmiana rozmiaru, wykrycie dłoni oraz jej konturów
def process_image(image):
    image = cv2.resize(image, (300,300))
    image = cv2.cvtColor(image,cv2.COLOR_BGR2HLS)
    image = cv2.inRange(image, (0, 20, 50), (30, 180, 150))
    image = cv2.blur(image, (12,12))
    _ , image = cv2.threshold(image, 100, 255, cv2.THRESH_BINARY)
    contours, _ = cv2.findContours(image,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    image = cv2.drawContours(image, contours, -1, (128,0,0), 5)
    return image, contours

img[0] = cv2.imread("/hands/scissors_front.jpg")
img[1] = cv2.imread("/hands/scissors_back.jpg")
img[2] = cv2.imread("/hands/paper_front.jpg")
img[3] = cv2.imread("/hands/paper_back.jpg")
img[4] = cv2.imread("/hands/spock_front.jpg")
img[5] = cv2.imread("/hands/spock_back.jpg")

for i in range(6):
    img[i],cnt[i] = process_image(img[i])


user_image = cv2.imread("test.jpg")
test_image, test_contours = process_image(user_image)

try:
    #jeśli istnieje drugi kontur to znaczy że w dłoni jest dziura, więc dłoń wskazuje jaszczurkę
    inner = test_contours[1]
    hand = "lizard"
except IndexError:
    #obliczanie krągłosci badanego konturu
    #Jeśli kontur jest krągły (W < 0.4), to jest to kamień
    L = cv2.arcLength(test_contours[0],0)
    S = cv2.contourArea(test_contours[0])
    W = L / ( 2 * math.sqrt(S * 3.14159)) - 1
    if (W < 0.4):
        hand = "rock"
    else:
        for i in range(6):
            #sprawdzanie podobieństwa
            similarity = cv2.matchShapes(test_contours[0], cnt[i][0], 2, 0.0)
            values[i]=similarity
            index_min = np.argmin(values)
        if (index_min == 0 or index_min == 1):
            hand = "scissors"
        elif (index_min == 2 or index_min == 3):
            hand = "paper"
        elif (index_min == 4 or index_min == 5):
            hand = "spock"

#przedstawienie wyników użytkownikowi
user_image = cv2.resize(user_image, (300,300))
user_image = cv2.drawContours(user_image, test_contours, -1, (150,0,0), 5)
M = cv2.moments(test_contours[0])
cX = int(M["m10"] / M["m00"])
cY = int(M["m01"] / M["m00"])
cv2.putText(user_image,hand,(cX - 20, cY + 20), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (150, 0, 0), 1, cv2.LINE_AA)
cv2.imwrite("result.jpg",user_image)

